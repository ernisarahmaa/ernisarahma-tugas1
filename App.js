import React from 'react';
import { 
  Image, 
  StyleSheet, 
  Text, 
  TextInput, 
  TouchableOpacity, 
  View, 
  Alert,
} from 'react-native';

const App = () => {  
  const [text, setText] = React.useState("");
  const [pass, setPass] = React.useState("");

  return (
    <View style={{backgroundColor: '#F7F7F7', flex: 1}}>
     <View style={styles.content}>
      <View style={{marginTop: -18}}>
        <Text style={styles.title}>Digital Approval</Text>
      </View>
      <Image style={styles.gambar} source={require('./assets/gambar.png')} />
      <View style={styles.inputContainer}>
        <Image style={styles.icon} source={require('./assets/email.png')} />
          <TextInput
            style={styles.input}
            placeholder="Alamat Email"
            label="Email"
            value={text}
            onChangeText={(text) => setText(text)}
          />
        </View>
        <View style={styles.inputContainer}>
        <Image style={styles.icon} source={require('./assets/lock.png')} />
          <TextInput
            style={styles.input}
            placeholder="Password"
            label="Password"
            value={pass}
            onChangeText={(pass) => setPass(pass)}
            secureTextEntry={true}
          />
        </View>
      <TouchableOpacity>
        <Text style={styles.reset}>Reset Password</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => Alert.alert('Berhasil Login!')}>
        <Text style={styles.login}>LOGIN</Text>
      </TouchableOpacity>
     </View>
    </View>
  );
};

const styles = StyleSheet.create({
  content: {
    marginTop: 200,
    backgroundColor: '#FFFFFF',
    width: 320,
    height: 381,
    alignSelf: 'center',
  },
  title: {
    backgroundColor: '#002558',
    padding: 8,
    textAlign: 'center',
    fontSize: 16,
    color: 'white',
    borderRadius: 100,
    width: 170,
    height: 36,
    alignSelf: 'center',
    fontWeight: 'bold',
  },
  gambar: {
    width: 127,
    height: 55,
    alignSelf: 'center',
    marginTop: 27,
    marginBottom: 38,
  },
  input: {
    height: 48,
    width: 288,
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#999',
    borderRadius: 5,
    height: 48,
    width: 288,
    alignSelf: 'center',
    marginBottom: 16,
  },
  icon: {
    marginLeft: 16,
    marginRight: 8,
    width: 16,
    height: 16,
  },
  reset: {
    fontStyle: 'italic',
    color: '#287AE5',
    textAlign: 'right',
    marginRight: 16,
  },
  login: {
    fontSize: 16,
    color: '#FFFFFF',
    backgroundColor: '#287AE5',
    width: 288,
    height: 48,
    alignSelf: 'center',
    textAlign: 'center',
    padding: 12,
    fontWeight: 'bold',
    borderRadius: 5,
    marginTop: 24,
  }
});

export default App;